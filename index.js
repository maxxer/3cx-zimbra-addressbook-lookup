/**
 * 3CX to Zimbra REST API - CRM integration
 * 
 * Author: Lorenzo Milesi <lorenzo@mile.si>
 */

const fastify = require('fastify')({ logger: true })

const fs = require("fs");
if (!fs.existsSync('./config.json')) {
    throw Error("Missing config.json")
}

global.gConfig = require('./config.json')
const zimbraSearch = require('./zimbra')

// Param validation
const validatePhoneNumber = {
    params: {
        type: 'object',
        properties: {
            phoneNumber: { type: 'number' }
        }
    }
}

// Declare a route
fastify.get('/v1/search/:phoneNumber', validatePhoneNumber, zimbraSearch)

// Run the server on localhost!
// If you want to make it listen to all ips, and you've read the README about security, change the following line with
// fastify.listen(1978, "::", function (err, address) {
fastify.listen(1978, function (err, address) {
    if (err) {
        fastify.log.error(err)
        process.exit(1)
    }
    fastify.log.info(`server listening on ${address}`)
})
