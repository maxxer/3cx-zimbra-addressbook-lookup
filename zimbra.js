/**
* Perform Zimbra SOAP API request
* 
* Note: I know this sucks, it's not leveraging Fastify nor Node. Forgive me,
* it's day 1 of "learning js the wrong way".
* 
* Author: Lorenzo Milesi <lorenzo@mile.si>
*/

var MD5 = require("crypto-js/md5");
var jszimbra = require('js-zimbra');
const csvparser = require('csv-parse')
const parser = csvparser({
    delimiter: ';',
    columns: true
})

var cron = require('node-schedule');
var rule = new cron.RecurrenceRule();
rule.minute = Math.floor((Math.random() * 59));
cron.scheduleJob("zimbraContactsUpdate", rule, function(){
    zimbraConnect()
});
console.log(cron.scheduledJobs)

var contact_list = []
var temporary_contact_list = []

// "global" variables
var comm;
var request;
var searchNumber;
var fReply;

/**
 * Look for the passed phone number in contact list array
 * @param {string} nr 
 */
async function lookupNumber(nr) {
    if (contact_list.length == 0) {
        return {}
    }
    nr = cleanPhoneNr(nr)
    var rexp = new RegExp('.*'+nr+'$')
    let obj = contact_list.find(o => {
        return o.workPhone.match(rexp) 
            || o.mobilephone.match(rexp)
    })
    console.log(obj)
    return obj
}

function zimbraConnect() {
    temporary_contact_list = []
    comm = new jszimbra.Communication({
        url: global.gConfig.zimbraUrl+"/service/soap"
    });
    comm.auth({
        "username": global.gConfig.zimbraUsername,
        "secret": global.gConfig.zimbraPassword,
        "isPassword": true
    }, function (err) {
        if (err) {
            console.log(err);
            throw new Error('Unable to connect to Zimbra');
        }
        // Instantiate request
        comm.getRequest({}, zimbraRequest)
    })
}

function zimbraRequest(err, req) {
    if (err) {
        console.log(err);
        throw new Error('Unable to instantiate getRequest');
    }
    request = req;
    
    // Perform the actual request
    // https://files.zimbra.com/docs/soap_api/8.8.15/api-reference/index.html
    request.addRequest({
        name: "ExportContactsRequest",
        namespace: "zimbraMail",
        params: {
            "ct": "csv",
            // FIXME handle multuple values!!
            "l": global.gConfig.zimbraContactsFolderIds,
            "csvsep": ";"
        }
    }, zimbraSend);
}

function zimbraSend(err) {
    if (err) {
        console.log(err);
        throw new Error('Unable to get request search');
    }
    
    comm.send(request, function (err, response) {
        if (err) {
            console.log(err);
            throw new Error('Unable to send search');
        }
        
        // response holds our response
        var out = response.get().ExportContactsResponse;
//        console.log("OUT");
//        console.log(out.content[0]._content);
        parser.write(out.content[0]._content);
        parser.end();
        /*
        if (out === undefined) {
            fReply.send({});
            return;
        }

        var found = lookupNumber(searchNumber)
        if (found === undefined) {
            fReply.send({});
            return;
        }
        fReply.send(found);
        */
        return;
    });
    
}

/**
 * Do some cleanup on the phone number so can be straight searchable by 3CX
 * @param {string} ph 
 */
function cleanPhoneNr(ph) {
    ph = ph.trim()
    // Replace leading + with 00
    ph = ph.replace(/^\+/, '00')
    // Remove spaces
    ph = ph.replace(/\s/g, '')
    // Remove crap
    ph = ph.replace(/\.\-/g, '')
    // Handle prefixes in parenthesis. Usually +39 (0341) 123 becomes +39341123 (not in Italy)
    ph = ph.replace(/\(\s*0(\d+)\)/g, '$1')
    return ph
}

/**
 * 3CX requires a unique ID for each contact. Generating one by hashing one of the 
 * available phones
 * @param {object} row 
 */
function generateContactId(row) {
    ph = row.workPhone ? row.workPhone : row.mobilephone
    return MD5(ph).toString()
}

// Use the readable stream api
parser.on('readable', function() {
    console.log("CSV reading loop")
    let record
    while (record = parser.read()) {
        if (record.workPhone == "" && record.mobilephone == "") {
            continue
        }
        // FIXME 3CX only has two phone fields. To have all the available numbers present
        // FIXME in Zimbra we could generate clone contacts with the additional numbers
        temporary_contact_list.push({
            company: record.company,
            workPhone: cleanPhoneNr(record.companyPhone),
            email: record.email,
            email2: record.email2, // ignored
            email3: record.email3, // ignored
            firstName: record.firstName,
            fullName: record.fullName,
            homePhone: cleanPhoneNr(record.homePhone), // ignored
            lastName: record.lastName,
            mobilephone: cleanPhoneNr(record.mobilePhone),
            otherPhone: cleanPhoneNr(record.otherPhone), // ignored
            workPhone: cleanPhoneNr(record.workPhone), // ignored
            workPhone2: cleanPhoneNr(record.workPhone2), // ignored
            id: generateContactId(record),
        })
    }
})
// Catch any error
parser.on('error', function(err) {
    console.error(err.message)
})
// CSV processing end - update "database"
parser.on('end', function() {
    console.log("Imported "+temporary_contact_list.length+" contacts");
    contact_list = temporary_contact_list
//    console.log(contact_list)
})

/**
* Main function calling the rest above
* @param {*} request 
* @param {*} reply 
*/
function zimbraContactSearch(request, reply) {
    searchNumber = request.params.phoneNumber;
    fReply = reply;
    lookupNumber(searchNumber)
        .then(nr => {
            if (nr === undefined) {
                fReply.send({});
                return;
            }
            fReply.send(nr)    
        })
    return;
}

// Build index array
zimbraConnect()

module.exports = zimbraContactSearch
