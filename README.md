# 3CX addressbook lookup for Zimbra contacts

This implements an API interface from 3CX PhoneSystem to Zimbra.

It works by performing a contacts export on a mailbox account. You will need to provide
a Zimbra account login for lookup, and configure (see below) the address books
to search in. Export is cached in a local array and refreshed hourly.

Phone numbers are *purified* with some basic rules (see `zimbra.js::cleanPhoneNr()`) to 
remove spaces and other visually comfortable characters, as 3CX provides a *plain* 
number for lookup (i.e. 123123123). 

## Installation

### Requirements

NodeJS and Yarn.

3CX Linux runs Debian9, if you install this on the same server you cannot use
system Node packages because they're too old. Find a guide or use 
[this](https://tecadmin.net/install-latest-nodejs-npm-on-debian/) to install
an up to date one.

### Get the sources

Clone this repo or download an archive, for instance into `/opt/3cx-zimbra-lookup`,
get into the folder and run

```
yarn install
```

## Configuration

Copy [`config.json.example`](config.json.example) to `config.json` and update it according
to your setup. The interface curently supports logging in with username and password only

### The `zimbraContactsFolderIds` param

This must list the contacts folder IDs where to search in. A way to get this is to run

```
zmmailbox -z -m REPLACE_WITH_zimbraUsername getAllFolders | grep " cont "
```

You can enter more than one ID comma separated (no spaces).

A future enhancement would be to scan for all contacts' folders and use them.

### Test run

```
node index.js
```

It will create a server listening on port 1978. You can now query the API by running

```
curl -s http://localhost:1978/v1/search/PHONENUMBER
```

### Start servce at boot

Copy the [`3cx-zimbra-lookup.service`](resources/3cx-zimbra-lookup.service) file to 
`/etc/systemd/system/` then run

```
chmod 644 /etc/systemd/system/3cx-zimbra-lookup.service
systemctl enable 3cx-zimbra-lookup
systemctl start 3cx-zimbra-lookup
```

You can monitor api output with `journalctl -u 3cx-zimbra-lookup`

## 3CX setup

You need at least a PRO license. Go to *Settings > CRM Integration* and upload the 
[xml template](resources/3cx-zimbra-crm-template.xml). Enter the API base url in 
the *General Configuration*.

## Security

There's **absolutely no security**! You have to:

* properly secure your `config.json` as there's a Zimbra password stored in plaintext
* block access to this api to everyone except the 3CX server, best option to install it there

## References

Uses [js-zimbra](https://github.com/Zimbra-Community/js-zimbra) for intefacing with SOAP APIs.
zimbraMail API [reference](https://files.zimbra.com/docs/soap_api/8.8.15/api-reference/index.html).

Developed using [Fastify](https://fastify.io) (even if could probably do without, but this 
project was my excuse to put a leg into js and this framework).

## Author

[Lorenzo "maxxer" Milesi](https://lorenzo.mile.si).

For bugs or enhancements or code suggestions visit [GitLab](https://gitlab.com/maxxer/3cx-zimbra-addressbook-lookup/-/issues).

See [LICENSE](LICENSE) for license details.
